package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Status;
import nl.kzaconnected.cursus.repository.StatusRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class StatusServiceTest {

    @TestConfiguration
    static class StatusServiceTestContextConfiguration {

        @Bean
        public StatusService statusService() {
            return new StatusService();
        }
    }

    @Autowired
    private StatusService statusService;
    @MockBean
    private StatusRepository statusRepository;

    private List<Status> statussen;

    @Before
    public void init() {
        statussen= new ArrayList<Status>();
        statussen.add(Status.builder().id(1L).status( "Status1").build());
        statussen.add(Status.builder().id(2L).status( "Status2").build());
    }

    @Test
    public void getAllStatussenShouldReturnCorrectListOfStatussen() {
        Mockito.when(statusRepository.findAll())
                .thenReturn(statussen);
        List<Status> found = statusService.findAll();
        for (Status status : found) {
            Assert.assertTrue(statussen.stream().anyMatch(status::equals));
        }
    }
}