// TODO Why does this block the email controller / swagger?
//package nl.kzaconnected.cursus.configuration;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//@EnableWebMvc
//public class WebConfig implements WebMvcConfigurer {
//
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowedOrigins("http://localhost:4200", "http://www.kzaconnected.nl", "http://cursusclient-connected.e4ff.pro-eu-west-1.openshiftapps.com")
//                .allowedMethods("POST", "GET",  "PUT", "OPTIONS", "DELETE")
//                .allowedHeaders("X-Auth-Token", "Content-Type", "Authorization")
//                .allowCredentials(false)
//                .maxAge(4800);
//    }
//}
