package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Cursist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import javax.mail.internet.MimeMessage;
import org.apache.commons.validator.routines.EmailValidator;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.mainemailadres}")
    private String mainEmailAdres;

    @Value("${spring.mail.senderemailadres}")
    private String senderEmailAdres;

    public String sendMail(String cursusNaam, Cursist cursist) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        if (isValidMail(cursist.getEmail())) {

            helper.setFrom(senderEmailAdres);
            helper.setTo(mainEmailAdres);
            helper.setCc(cursist.getEmail());
            helper.setSubject("Aanmelding cursus");
            helper.setText("Hallo Chantal," +
                    "\n \n Graag wil ik mij aanmelden voor de volgende cursus:" +
                    "\n " + cursusNaam +
                    "\n \n Gr.," +
                    "\n " + cursist.getNaam());
            try {
                this.sender.send(message);
                return "Mail is succesvol verzonden.";
            } catch (Exception ex) {
                throw new RuntimeException("Mail is niet succesvol verzonden. " + ex);
            }
        } else {
            throw new IllegalArgumentException("Email adres moet geldig zijn.");
        }
    }

    private boolean isValidMail (String mailAdres) {
        return EmailValidator.getInstance().isValid(mailAdres);
    }
}
